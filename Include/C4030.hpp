/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4030: public nts::AComponent {
private:
	IComponent *_pin[14];
	nts::PinType _pinType[14];
	std::pair<size_t, size_t> _link[14];
	
	std::pair<nts::Tristate, nts::Tristate>
	computeReceiverPair(std::size_t pin);
public:
	C4030();
	virtual ~C4030();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};