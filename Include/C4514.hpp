/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4514: public nts::AComponent {
private:
	std::size_t _pinNbr;
	std::size_t _outputGate[16];
	IComponent *_pin[24];
	IComponent *_first;
	nts::PinType _pinType[24];
	nts::Tristate _previous;
	std::pair<size_t, size_t> _link[24];
	
	std::size_t outputGateNbr();
public:
	C4514();
	virtual ~C4514();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};