/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <string>
#include <vector>

namespace FileHelper {
	std::string readFile(const std::string);
	std::vector<std::string> readFileAsVector(const std::string);
};