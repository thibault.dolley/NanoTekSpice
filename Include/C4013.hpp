/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4013: public nts::AComponent {
private:
	std::size_t _pinNbr;
	IComponent *_pin[14];
	nts::PinType _pinType[14];
	std::pair<size_t, size_t> _link[14];

	bool isComputable(std::size_t i);
	nts::Tristate computeQ(std::size_t i);

public:
	C4013();
	virtual ~C4013();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};