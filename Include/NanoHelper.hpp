/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <cstddef>

namespace NanoHelper {
	std::size_t loop();
	void initLoop();
	void endLoop();
	void stopLoop(int signal);
};