/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <string>
#include <stdexcept>

namespace nts {
	class Error: public std::exception {
	private:
		std::string _msg;
	public:
		Error(std::string const &s);
		~Error();
		virtual const char *what() const throw();
	};

	class LogicError: public Error {
	private:
		std::string _msg;
	public:
		LogicError(std::string const &s);
		~LogicError();
	};
	
	class InvalidArg: public Error {
	private:
		std::string _msg;
	public:
		InvalidArg(std::string const &s);
		~InvalidArg();
	};

};