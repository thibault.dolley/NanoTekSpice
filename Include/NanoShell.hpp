/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <string>
#include <functional>
#include "NanoTekSpice.hpp"

#define SHELL_BIND(x) std::bind(x, this)

class NanoShell {
private:
	NanoTekSpice _nano;
	std::map<std::string, std::function<int ()>> _shellCall;

	int loop();
	int exit();
	int dump();
	int display();
	int simulate();
	bool inputExist(std::string);
	void setInputValue(std::string);
public:
	NanoShell(NanoTekSpice &nano);
	~NanoShell();
	
	void runShell();
};