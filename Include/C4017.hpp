/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4017: public nts::AComponent {
private:
	std::size_t _pinNbr;
	std::size_t _counter;
	std::map<size_t, size_t> _output;
	IComponent *_pin[15];
	IComponent *_first;
	nts::PinType _pinType[15];
	std::pair<size_t, size_t> _link[15];

	nts::Tristate computeC4017(std::size_t i);

public:
	C4017();
	virtual ~C4017();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};