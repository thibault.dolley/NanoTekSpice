/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <vector>
#include <string>
#include "Input.hpp"
#include "Clock.hpp"
#include "Output.hpp"
#include "IComponent.hpp"
#include "ComponentManager.hpp"

class NanoTekSpice {
private:
	std::map<std::string, Clock *> _clock;
	std::map<std::string, Input *> _input;
	std::map<std::string, Output *> _output;
	std::map<std::string, nts::IComponent *> _components;

	void checkInput();
	void checkOutput();
	void setupInput(int, char **);
	void bindLinks(std::vector<std::string>);
	void buildComponent(ComponentManager &, std::vector<std::string>);
	std::size_t buildComponentsGraph(std::vector<std::string>);
	std::size_t linkComponents(std::size_t, std::vector<std::string>);
	void unLinkComponent(const std::string &s1,
				const std::string &s2,
				const int pin1,
				const int pin2);
public:
	NanoTekSpice(int, char**);
	~NanoTekSpice();
	
	void dump();
	void invertClock();
	void displayOutput();
	void launchSimulation();
	void setInput(std::string, int);
	bool inputExist(const std::string);
};