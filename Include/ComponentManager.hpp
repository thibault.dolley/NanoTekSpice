/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <string>
#include <memory>
#include <functional>
#include "IComponent.hpp"

#define MANAGER_BIND(x) std::bind(x, this, std::placeholders::_1)
#define IC_BUILDER std::function<nts::IComponent *(const std::string &)>

class ComponentManager {
private:
	std::map<std::string, IC_BUILDER> _manager;

	nts::IComponent *createTrue(const std::string&) const noexcept;
	nts::IComponent *createClock(const std::string&) const noexcept;
	nts::IComponent *createInput(const std::string&) const noexcept;
	nts::IComponent *createFalse(const std::string&) const noexcept;
	nts::IComponent *createC4001(const std::string&) const noexcept;
	nts::IComponent *createC4008(const std::string&) const noexcept;
	nts::IComponent *createC4011(const std::string&) const noexcept;
	nts::IComponent *createC4013(const std::string&) const noexcept;
	nts::IComponent *createC4017(const std::string&) const noexcept;
	nts::IComponent *createC4030(const std::string&) const noexcept;
	nts::IComponent *createC4040(const std::string&) const noexcept;
	nts::IComponent *createC4069(const std::string&) const noexcept;
	nts::IComponent *createC4071(const std::string&) const noexcept;
	nts::IComponent *createC4081(const std::string&) const noexcept;
	nts::IComponent *createC4512(const std::string&) const noexcept;
	nts::IComponent *createC4514(const std::string&) const noexcept;
	nts::IComponent *createOutput(const std::string&) const noexcept;
public:
	ComponentManager();
	~ComponentManager();

	std::unique_ptr<nts::IComponent> createComponent(const std::string&,
							const std::string&);
};