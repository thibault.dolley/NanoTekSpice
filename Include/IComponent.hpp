/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <cstddef>

namespace nts {
	enum Tristate {
		UNDEFINED 	= (-true),
		TRUE 		= true,
		FALSE 		= false
	};
	class IComponent {
		public:
		virtual ~IComponent() = default;

		virtual void dump() const = 0;
		virtual nts::Tristate compute(std::size_t pin = 1) = 0;
		virtual void setLink(std::size_t, nts::IComponent &,
			std::size_t) = 0;
	};
};