/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <cstddef>
#include "IComponent.hpp"

#define AC(x) static_cast<nts::AComponent*>(x)

namespace nts {
	enum PinType {
		NONE,
		TRANSMITTER,
		RECEIVER
	};
	class AComponent: public nts::IComponent {
		public:
		virtual ~AComponent() = default;

		virtual void unLink(std::size_t pin = 1) = 0;
		virtual nts::PinType getPinType(std::size_t pin = 1) const = 0;
	};
};