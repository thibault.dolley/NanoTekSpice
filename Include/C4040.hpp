/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4040: public nts::AComponent {
private:
	std::size_t _pinNbr;
	std::size_t _counter;
	std::map<size_t, size_t> _output;
	IComponent *_pin[16];
	IComponent *_first;
	nts::PinType _pinType[16];
	std::pair<size_t, size_t> _link[16];

	nts::Tristate computeC4040(std::size_t i);

public:
	C4040();
	virtual ~C4040();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};