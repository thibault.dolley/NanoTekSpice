/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4512: public nts::AComponent {
private:
	IComponent *_pin[16];
	nts::PinType _pinType[16];
	std::pair<size_t, size_t> _link[16];

	std::size_t outputGateNbr();
public:
	C4512();
	virtual ~C4512();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};