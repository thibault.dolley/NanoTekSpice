/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4069: public nts::AComponent {
private:
	IComponent *_pin[14];
	nts::PinType _pinType[14];
	std::pair<size_t, size_t> _link[14];
public:
	C4069();
	virtual ~C4069();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};