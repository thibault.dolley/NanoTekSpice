/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#pragma once

#include <map>
#include <utility>
#include "AComponent.hpp"
#include "IComponent.hpp"

class C4008: public nts::AComponent {
private:
	std::size_t _pinNbr;
	IComponent *_pin[16];
	nts::PinType _pinType[16];
	std::pair<size_t, size_t> _link[16];
	std::map<size_t, std::pair<size_t, size_t>> _inputPin;

	bool isComputable(std::size_t i);
	nts::Tristate computeSum(std::size_t i);
	nts::Tristate computeCarryOut(std::size_t i);
public:
	C4008();
	virtual ~C4008();

	void dump() const override;
	void unLink(std::size_t pin = 1) override;
	nts::Tristate compute(std::size_t pin = 1) override;
	nts::PinType getPinType(std::size_t pin = 1) const override;
	void setLink(std::size_t, nts::IComponent &, std::size_t) override;
};