/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <map>
#include <vector>
#include <iostream>
#include "Input.hpp"
#include "Clock.hpp"
#include "NtsErrors.hpp"
#include "Output.hpp"
#include "FileHelper.hpp"
#include "IComponent.hpp"
#include "StringHelper.hpp"
#include "NanoTekSpice.hpp"
#include "ComponentManager.hpp"

NanoTekSpice::NanoTekSpice(int nbParam, char **param)
{
	(void)nbParam;
	std::vector<std::string> file = FileHelper::readFileAsVector(param[0]);
	StringHelper::epureStringVector(file);
	std::size_t i = this->buildComponentsGraph(file);
	this->linkComponents(i, file);
	this->setupInput(nbParam, param);
	this->checkInput();
	this->checkOutput();
}

NanoTekSpice::~NanoTekSpice()
{
}

void NanoTekSpice::checkOutput()
{
	std::map<std::string, Output*>::iterator it = _output.begin();	

	for (; it != _output.end(); it++)
		if (!it->second->isPined())
			throw nts::LogicError("An output is not pined.");
}

void NanoTekSpice::checkInput()
{
	std::map<std::string, Clock*>::iterator itClock = _clock.begin();
	std::map<std::string, Input*>::iterator itInput = _input.begin();
	std::size_t error = 0;
	for (; itClock != _clock.end(); itClock++) {
		if (itClock->second->compute() == nts::UNDEFINED) {
			std::cerr << "Clock " + itClock->first +
			" is undefined." << std::endl;
			error++;
		}
	}
	for (; itInput != _input.end(); itInput++) {
		if (itInput->second->compute() == nts::UNDEFINED) {
			std::cerr << "Input " + itInput->first +
			" is undefined." << std::endl;
			error++;
		}
	}
	if (error)
		throw nts::InvalidArg("Some component are still undefined.");
}

void NanoTekSpice::setupInput(int nbParam, char **param)
{
	for (int i = 1; i < nbParam; i++) {
		std::vector<std::string> inputValue =
				StringHelper::explodeString(param[i], '=');
		if (inputValue.size() != 2)
			throw nts::InvalidArg("Invalid parameter.");
		if (_input.find(inputValue[0]) != _input.end())
			_input[inputValue[0]]->setValue(stoi(inputValue[1]));
		else if (_clock.find(inputValue[0]) != _clock.end())
			_clock[inputValue[0]]->setValue(stoi(inputValue[1]));
		else
			throw nts::InvalidArg(inputValue[0] +
				": no such input.");
	}
}

void NanoTekSpice::unLinkComponent(const std::string &s1,
				const std::string &s2,
				const int pin1,
				const int pin2)
{
	AC(_components[s1])->unLink(pin1);
	AC(_components[s2])->unLink(pin2);
}

void NanoTekSpice::bindLinks(std::vector<std::string> links)
{
	std::vector<std::string> compo1 =
		StringHelper::explodeString(links[0], ':');
	std::vector<std::string> compo2 =
		StringHelper::explodeString(links[1], ':');

	if (compo1.size() != 2 || compo2.size() != 2)
		throw nts::InvalidArg("Invalid syntax for links.");
	if (_components.find(compo1[0]) == _components.end() ||
		_components.find(compo2[0]) == _components.end())
		throw nts::LogicError("Can't link to unexisting component");
	int pin1 = stoi(compo1[1]);
	int pin2 = stoi(compo2[1]);
	nts::PinType pinType1 = AC(_components[compo1[0]])->getPinType(pin1);
	nts::PinType pinType2 = AC(_components[compo2[0]])->getPinType(pin2);
	if (pinType1 == pinType2)
		throw nts::LogicError("Can't bind two pin of the same type.");
	if (pinType1 == nts::NONE || pinType2 == nts::NONE)
		throw nts::LogicError("Binding to a obsolete pin.");
	this->unLinkComponent(compo1[0], compo2[0], pin1, pin2);
	_components[compo1[0]]->setLink(pin1, *_components[compo2[0]], pin2);
	_components[compo2[0]]->setLink(pin2, *_components[compo1[0]], pin1);
}

std::size_t
NanoTekSpice::linkComponents(std::size_t i, std::vector<std::string> file)
{
	std::size_t errorCount = 0;
	if (i >= file.size() || file[i++] != ".links:")
		throw nts::InvalidArg("No '.links:' in the nts file.");
	for (; i < file.size(); i++) {
		if (file[i] != "") {
			std::vector<std::string> links =
				StringHelper::explodeString(file[i], ' ');
			if (links.size() != 2)
				throw nts::LogicError("Links need 2 compo.");
			try { this->bindLinks(links); }
			catch (const std::exception &e) {
				std::cerr << "line " << i + 1 << ": "
				<< e.what() << std::endl;
				errorCount++;
			}
		}
	}
	if (errorCount)
		throw nts::InvalidArg("linkage failed.");
	return (i);
}

void NanoTekSpice::buildComponent(ComponentManager &componentManager,
				std::vector<std::string> spec)
{
	if (spec.size() != 2)
		throw nts::InvalidArg("Invalid component spec.");
	if (this->_components.find(spec[1]) != this->_components.end())
		throw nts::LogicError("Component already created.");

	this->_components[spec[1]] =
		componentManager.createComponent(spec[0], "").release();
	if (spec[0] == "input")
		_input[spec[1]] = (Input*)_components[spec[1]];
	else if (spec[0] == "output")
		_output[spec[1]] = (Output*)_components[spec[1]];
	if (spec[0] == "clock")
		_clock[spec[1]] = (Clock*)_components[spec[1]];
}

std::size_t NanoTekSpice::buildComponentsGraph(std::vector<std::string> file)
{
	std::size_t i = 0;
	std::size_t error = 0;
	ComponentManager componentManager;
	for (; i < file.size() && file[i] == ""; i++);
	if (file[i++] != ".chipsets:")
		throw nts::InvalidArg("Garbage before '.chipset:'");
	for (; i < file.size() && file[i] != ".links:"; i++) {
		if (file[i] != "") {
			std::vector<std::string> component =
				StringHelper::explodeString(file[i], ' ');
			try { buildComponent(componentManager, component); }
			catch (const std::exception &e) {
				std::cerr << "line " << i + 1 << ": "
				<< e.what() << std::endl;
				error++;
			}
		}
	}
	if (error)
		throw nts::InvalidArg("Build failed.");
	return i;
}

void NanoTekSpice::launchSimulation()
{
	std::map<std::string, Output*>::iterator it = _output.begin();	

	for (; it != _output.end(); it++)
		it->second->compute();
}

void NanoTekSpice::displayOutput()
{
	std::map<std::string, Output*>::iterator it = _output.begin();	

	for (; it != _output.end(); it++) {
		int state = it->second->getState();

		std::cout << it->first << "=";
		if (state == -1)
			std::cout << 'U' << std::endl;
		else
			std::cout << state << std::endl;
	}
}

void NanoTekSpice::dump()
{
	std::map<std::string, nts::IComponent*>::iterator it =
							_components.begin();	

	for (; it != _components.end(); it++) {
		std::cout << it->first << " : ";
		it->second->dump();
	}
}

void NanoTekSpice::invertClock()
{
	std::map<std::string, Clock*>::iterator it =
							_clock.begin();	

	for (; it != _clock.end(); it++)
		it->second->invertValue();
}

void NanoTekSpice::setInput(std::string inputName, int state)
{
	if (_input.find(inputName) == _input.end())
		throw nts::InvalidArg("This input does not exist");
	_input[inputName]->setValue(state);
}

bool NanoTekSpice::inputExist(const std::string str)
{
	if (_input.find(str) == _input.end())
		return false;
	return true; 
}
