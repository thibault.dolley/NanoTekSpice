/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include "NtsErrors.hpp"

nts::InvalidArg::InvalidArg(std::string const &s): nts::Error(s)
{
}

nts::InvalidArg::~InvalidArg()
{
}