/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include "NtsErrors.hpp"

nts::Error::Error(std::string const &s): _msg(s)
{
}

nts::Error::~Error()
{
}

const char *nts::Error::what() const throw()
{
	return _msg.c_str();
}
