/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "NanoShell.hpp"
#include "NanoTekSpice.hpp"
#include "ComponentManager.hpp"

int main(int argc, char **argv)
{
	if (argc == 1)
		return (84);
	try {
		NanoTekSpice nano(argc - 1, &argv[1]);
		nano.launchSimulation();
		nano.displayOutput();
		nano.invertClock();
		NanoShell shell(nano);
		shell.runShell();
	} catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return (84);
	}
	return (0);
}
