/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <string>
#include <iostream>
#include <stdexcept>
#include "NanoShell.hpp"
#include "NtsErrors.hpp"
#include "NanoHelper.hpp"
#include "StringHelper.hpp"
#include "NanoTekSpice.hpp"

NanoShell::NanoShell(NanoTekSpice &nano):_nano(nano)
{
	_shellCall["dump"] = SHELL_BIND(&NanoShell::dump);
	_shellCall["loop"] = SHELL_BIND(&NanoShell::loop);
	_shellCall["exit"] = SHELL_BIND(&NanoShell::exit);
	_shellCall["simulate"] = SHELL_BIND(&NanoShell::simulate);
	_shellCall["display"] = SHELL_BIND(&NanoShell::display);
}

NanoShell::~NanoShell()
{
}

void NanoShell::runShell()
{
	std::string cmd;

	std::cout << "> ";
	while (std::getline(std::cin, cmd)) {
		if (_shellCall.find(cmd) != _shellCall.end()) {
			if (_shellCall[cmd]())
				break;
		} else if (this->inputExist(cmd)) {
			try {
				this->setInputValue(cmd);
			} catch (std::exception &e) {
				std::cout << "can't Change input : " << 
				e.what() << std::endl;
			}
		}
		else
			std::cout << "invalid command." << std::endl;
		std::cout << "> ";
	}
}

bool NanoShell::inputExist(std::string str)
{
	std::vector<std::string> value = StringHelper::explodeString(str, '=');

	return _nano.inputExist(value[0]);
}

void NanoShell::setInputValue(std::string str)
{
	std::vector<std::string> value = StringHelper::explodeString(str, '=');
	int i = -1;

	if (value.size() != 2)
		throw nts::InvalidArg("Not enough paramater");
	try {
		i = stoi(value[1]);
	} catch (std::exception &e) {
		throw nts::InvalidArg("Stoi failed");
	}
	_nano.setInput(value[0], i);
}

int NanoShell::dump()
{
	_nano.dump();
	return 0;
}

int NanoShell::exit()
{
	return 1;
}

int NanoShell::simulate()
{
	_nano.launchSimulation();
	_nano.invertClock();
	return 0;
}

int NanoShell::display()
{
	_nano.displayOutput();
	return 0;
}

int NanoShell::loop()
{
	NanoHelper::initLoop();
	while (NanoHelper::loop()) {
		_nano.launchSimulation();
		_nano.invertClock();
	}
	NanoHelper::endLoop();
	return 0;
}
