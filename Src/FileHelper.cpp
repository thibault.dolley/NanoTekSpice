/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <vector>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include "NtsErrors.hpp"
#include "FileHelper.hpp"

std::string FileHelper::readFile(const std::string fileName)
{
	std::ifstream file(fileName);
	std::ostringstream buffer;

	if (file.is_open()) {
		buffer << file.rdbuf();
		file.close();
		return (buffer.str());
	}
	throw nts::InvalidArg("The file passed can't be read.");
}

std::vector<std::string> FileHelper::readFileAsVector(const std::string f)
{
	std::string line;    
	std::ifstream file(f);
	std::vector<std::string> lines;

	if (file.is_open()) {
		while(getline(file, line))
			lines.push_back(line);
		return lines;
	}
	throw nts::InvalidArg("The file passed can't be read.");
}