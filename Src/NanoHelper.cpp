/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <string>
#include <csignal>
#include "NanoHelper.hpp"

static std::size_t g_loop = 1;

void NanoHelper::stopLoop(int signal)
{
	(void)signal;
	g_loop = 0;
}

void NanoHelper::initLoop()
{
	g_loop = 1;
	signal(SIGINT, NanoHelper::stopLoop);
}

std::size_t NanoHelper::loop()
{
	return g_loop;
}

void NanoHelper::endLoop()
{
	g_loop = 0;
	signal(SIGINT, SIG_DFL);
}