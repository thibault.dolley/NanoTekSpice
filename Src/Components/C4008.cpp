/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4008.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4008::C4008()
{
	_pinNbr = 16;
	for (unsigned int i = 0; i < _pinNbr; i++) {
		_pin[i] = nullptr;
		if (i == 7 || i == 15)
			_pinType[i] = nts::NONE;
		else if (i <= 8 || i == 14) {
			_pinType[i] = nts::RECEIVER;
		}
		else
			_pinType[i] = nts::TRANSMITTER;
	}
	_inputPin[13] = std::make_pair(1, 15);
	_inputPin[12] = std::make_pair(3, 2);
	_inputPin[11] = std::make_pair(5, 4);
	_inputPin[10] = std::make_pair(6, 7);
}

C4008::~C4008()
{
}

void C4008::dump() const
{
	std::cout << "C4008" << std::endl;
}

/*
** ALSJLKQJDKLQSJLDKSQDSJQKL
** if (_pin[i] == nullptr || _pin[i] == this) 
** CETTE CONDITIONE EST LAZY OK ????
** SI LA MOULI FAIL C EST A CAUSE DE CETTE CONDITION
** TOUT PAREIL DANS COMPUTECARRYOUT
*/

bool C4008::isComputable(std::size_t i)
{
	if (_pin[i - 1] == nullptr)
		return false;
	if (_pin[i - 1] == this) {
		if (_link[i - 1].second == 13 || _link[i - 1].second == 14)
			return false;
		if (_link[i - 1].second == 12 && (i >= 2 && i <= 9))
			return false;
		if (_link[i - 1].second == 11 && (i >= 4 && i <= 9))
			return false;
		if (_link[i - 1].second == 10 && (i >= 6 && i <= 9))
			return false;
	}
	return true;
}

nts::Tristate C4008::computeCarryOut(std::size_t i)
{
	nts::Tristate cIn = this->compute(9);
	for (std::size_t j = 10; j <= 13 && j <= i; j++) {
		std::size_t result = 0;
		nts::Tristate inputA = this->compute(_inputPin[j].first);
		nts::Tristate inputB = this->compute(_inputPin[j].second);
		if (inputA == nts::UNDEFINED ||
			inputB == nts::UNDEFINED ||
			cIn == nts::UNDEFINED)
			return nts::UNDEFINED;
		result += (cIn == nts::TRUE) ? 1 : 0;
		result += (inputA == nts::TRUE) ? 1 : 0;
		result += (inputB == nts::TRUE) ? 1 : 0;
		cIn = nts::FALSE;
		if (result > 1)
			cIn = nts::TRUE;
	}
	return cIn;
}

nts::Tristate C4008::computeSum(std::size_t i)
{
	size_t result = 0;
	nts::Tristate cIn;

	cIn = this->computeCarryOut(i - 1);
	nts::Tristate inputA = this->compute(_inputPin[i].first);
	nts::Tristate inputB = this->compute(_inputPin[i].second);

	if (inputA == nts::UNDEFINED ||
		inputB == nts::UNDEFINED ||
		cIn == nts::UNDEFINED)
		return nts::UNDEFINED;
	result += (cIn == nts::TRUE) ? 1 : 0;
	result += (inputA == nts::TRUE) ? 1 : 0;
	result += (inputB == nts::TRUE) ? 1 : 0;
	if (!result || result == 2)
		return nts::FALSE;
	return nts::TRUE;
}

nts::Tristate C4008::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (!this->isComputable(i + 1))
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	if (i + 1 == 14)
		return this->computeCarryOut(i);
	return this->computeSum(i + 1);
}

void C4008::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4008::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4008::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}