/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "Clock.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"

Clock::Clock()
{
	_pin = nullptr;
	_value = nts::UNDEFINED;
}

Clock::~Clock()
{
}

void Clock::dump() const
{
	std::cout << "Clock" << std::endl;
}

nts::Tristate Clock::compute(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	return this->_value;
}

void Clock::unLink(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	_pin = nullptr;
	_link = std::make_pair(0, 0);
}

void Clock::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin)
		throw nts::LogicError("Pin already binded.");
	_pin = &other;
	_link = std::make_pair(pin, oPin);
}

nts::PinType Clock::getPinType(std::size_t pin) const
{
	(void)pin;
	return nts::TRANSMITTER;
}

void Clock::setValue(const int i)
{
	if (i == 0)
		_value = nts::FALSE;
	if (i == 1)
		_value = nts::TRUE;
}

void Clock::invertValue()
{
	if (this->_value == nts::TRUE)
		this->_value = nts::FALSE;
	else if (this->_value == nts::FALSE)
		this->_value = nts::TRUE;
}
