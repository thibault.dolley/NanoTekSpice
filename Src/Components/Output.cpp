/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "Output.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"

Output::Output()
{
	_pin = nullptr;
	_value = nts::UNDEFINED;
}

Output::~Output()
{
}

void Output::dump() const
{
	std::cout << "Output" << std::endl;
}

nts::Tristate Output::compute(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (!_pin)
		return nts::UNDEFINED;
	_value = this->_pin->compute(_link.second);
	return _value;
}

void Output::unLink(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	_pin = nullptr;
	_link = std::make_pair(0, 0);
}

void Output::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin)
		throw nts::LogicError("Pin already binded.");
	_pin = &other;
	_link = std::make_pair(pin, oPin);
}

nts::PinType Output::getPinType(std::size_t pin) const
{
	(void)pin;
	return nts::RECEIVER;
}

int Output::getState() const
{
	if (_value == nts::UNDEFINED)
		return -1;
	else if (_value == nts::FALSE)
		return 0;
	return 1;
}

bool Output::isPined() const
{
	return _pin;
}
