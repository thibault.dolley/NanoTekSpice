/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4040.hpp"
#include "NtsErrors.hpp"
#include "IComponent.hpp"

C4040::C4040()
{
	_pinNbr = 16;
	_counter = 0;
	for (unsigned int i = 0; i < _pinNbr; i++) {
		_pin[i] = nullptr;
		if (i == 15 || i == 7)
			_pinType[i] = nts::NONE;
		else if (i == 9 || i == 10)
			_pinType[i] = nts::RECEIVER;
		else
			_pinType[i] = nts::TRANSMITTER;
	}
	_first = nullptr;
	_output[9] = 1;
	_output[7] = 2;
	_output[6] = 3;
	_output[5] = 4;
	_output[3] = 5;
	_output[2] = 6;
	_output[4] = 7;
	_output[13] = 8;
	_output[12] = 9;
	_output[14] = 10;
	_output[15] = 11;
	_output[1] = 12;
}

C4040::~C4040()
{
}

void C4040::dump() const
{
	std::cout << "C4040" << std::endl;
}

nts::Tristate C4040::computeC4040(std::size_t i)
{
	nts::Tristate clock = this->compute(10);
	nts::Tristate reset = this->compute(11);

	if (reset == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (reset == nts::TRUE) {
		_counter = 0;
		return nts::FALSE;
	}
	if (clock == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (clock == nts::FALSE)
		_counter++;
	if ((_counter >> (_output[i] - 1)) & 1)
		return nts::TRUE;
	return nts::FALSE;
}

nts::Tristate C4040::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (_pin[i] == nullptr || _pin[i] == this)
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	if (_first == nullptr) {
		_first = _pin[i];
		return nts::FALSE;
	}
	if (_first != _pin[i]) {
		if ((_counter >> (_output[i + 1] - 1)) & 1)
			return nts::TRUE;
		return nts::FALSE;
	}
	return this->computeC4040(i + 1);
}

void C4040::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4040::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4040::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}