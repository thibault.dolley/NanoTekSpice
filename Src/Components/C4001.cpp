/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4001.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4001::C4001()
{
	for (unsigned int i = 0; i < 14; i++) {
		_pin[i] = nullptr;
		if (i == 13 || i == 6)
			_pinType[i] = nts::NONE;
		else if (i == 2 || i == 3 || i == 9 || i == 10)
			_pinType[i] = nts::TRANSMITTER;
		else
			_pinType[i] = nts::RECEIVER;
	}
}

C4001::~C4001()
{
}

void C4001::dump() const
{
	std::cout << "C4001" << std::endl;
}

std::pair<nts::Tristate, nts::Tristate>
C4001::computeReceiverPair(std::size_t pin)
{
	if (pin == 3)
		return std::make_pair(this->compute(1), this->compute(2));
	if (pin == 4)
		return std::make_pair(this->compute(5), this->compute(6));
	if (pin == 10)
		return std::make_pair(this->compute(8), this->compute(9));
	if (pin == 11)
		return std::make_pair(this->compute(12), this->compute(13));
 	return std::make_pair(nts::UNDEFINED, nts::UNDEFINED);	
}

nts::Tristate C4001::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (_pin[i] == nullptr)
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	std::pair<nts::Tristate, nts::Tristate> pair =
						this->computeReceiverPair(++i);
	if (pair.first == nts::FALSE && pair.second == nts::FALSE)
		return nts::TRUE;
	if (pair.first == nts::UNDEFINED || pair.second == nts::UNDEFINED)
		return nts::UNDEFINED;
	return nts::FALSE;
}

void C4001::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 14)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4001::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 14)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4001::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > 14)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}