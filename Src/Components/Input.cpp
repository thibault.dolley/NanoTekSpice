/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "Input.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"

Input::Input()
{
	_pin = nullptr;
	_value = nts::UNDEFINED;
}

Input::~Input()
{
}

void Input::dump() const
{
	std::cout << "Input" << std::endl;
}

nts::Tristate Input::compute(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	return this->_value;
}

void Input::unLink(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	_pin = nullptr;
	_link = std::make_pair(0, 0);
}

void Input::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin)
		throw nts::LogicError("Pin already binded.");
	_pin = &other;
	_link = std::make_pair(pin, oPin);
}

nts::PinType Input::getPinType(std::size_t pin) const
{
	(void)pin;
	return nts::TRANSMITTER;
}

void Input::setValue(const int i)
{
	if (i == 0)
		_value = nts::FALSE;
	if (i == 1)
		_value = nts::TRUE;
}
