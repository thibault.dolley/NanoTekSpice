/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4013.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4013::C4013()
{
	_pinNbr = 14;
	for (unsigned int i = 0; i < _pinNbr; i++) {
		_pin[i] = nullptr;
		if (i == 13 || i == 6)
			_pinType[i] = nts::NONE;
		else if (i < 2 || i > 10)
			_pinType[i] = nts::TRANSMITTER;
		else
			_pinType[i] = nts::RECEIVER;
	}
}

C4013::~C4013()
{
}

void C4013::dump() const
{
	std::cout << "C4013" << std::endl;
}

nts::Tristate C4013::computeQ(std::size_t i)
{
	nts::Tristate set = this->compute((i <= 2) ? 6 : 8);
	nts::Tristate data = this->compute((i <= 2) ? 5 : 9);
	nts::Tristate reset = this->compute((i <= 2) ? 4 : 10);
	nts::Tristate clock = this->compute((i <= 2) ? 3 : 11);

	if (set == nts::UNDEFINED || reset == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (set == nts::TRUE && reset == nts::TRUE)
		return nts::TRUE;
	if (set == nts::TRUE && reset == nts::FALSE)
		return ((i % 2 == 1) ? nts::TRUE : nts::FALSE); 
	if (set == nts::FALSE && reset == nts::TRUE)
		return ((i % 2 == 0) ? nts::TRUE : nts::FALSE); 
	if (clock == nts::UNDEFINED ||
		clock == nts::FALSE || 
		data == nts::UNDEFINED)
		return nts::UNDEFINED; 
	if (data == nts::TRUE)
		return ((i % 2 == 1) ? nts::TRUE : nts::FALSE);
	return ((i % 2 == 1) ? nts::FALSE : nts::TRUE);
}

bool C4013::isComputable(std::size_t i)
{
	i--;
	if (_pin[i] == nullptr)
		return false;
	if (_pin[i] == this) {
		std::size_t oPin = _link[i].second;

		if (oPin <= 2 && i <= 6)
			return false;
		if ((oPin == 12 || oPin == 13) && i > 7)
			return false;
	}
	return true;
}

nts::Tristate C4013::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (!this->isComputable(i + 1))
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	nts::Tristate result = this->computeQ(i + 1);
	return result;
}

void C4013::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4013::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4013::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}