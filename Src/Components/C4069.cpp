/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4069.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4069::C4069()
{
	for (unsigned int i = 0; i < 14; i++) {
		_pin[i] = nullptr;
		if (i == 6 || i == 13)
			_pinType[i] = nts::NONE;
		else if (i % 2)
			_pinType[i] = nts::TRANSMITTER;
		else
			_pinType[i] = nts::RECEIVER;
	}
}

C4069::~C4069()
{
}

void C4069::dump() const
{
	std::cout << "C4069" << std::endl;
}

nts::Tristate C4069::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);

	if (type == nts::NONE)
		return nts::UNDEFINED;
	else if (type == nts::RECEIVER) {
		if (_pin[i] == nullptr)
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	i++;
	nts::Tristate state = this->compute(i + ((i < 7) ? -1 : 1));
	if (state == nts::UNDEFINED)
		return state;
	else if (state == nts::TRUE)
		return nts::FALSE;
	return nts::TRUE;
}

void C4069::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 14)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4069::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 14)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4069::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > 14)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}