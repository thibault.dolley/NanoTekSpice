/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "False.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"

False::False()
{
	_pin = nullptr;
}

False::~False()
{
}

void False::dump() const
{
	std::cout << "False" << std::endl;
}

nts::Tristate False::compute(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	return nts::Tristate::FALSE;
}

void False::unLink(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	_pin = nullptr;
	_link = std::make_pair(0, 0);
}

void False::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin)
		throw nts::LogicError("Pin already binded.");
	_pin = &other;
	_link = std::make_pair(pin, oPin);
}

nts::PinType False::getPinType(std::size_t pin) const
{
	(void)pin;
	return nts::TRANSMITTER;
}