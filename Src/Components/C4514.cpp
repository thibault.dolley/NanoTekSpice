/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4514.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4514::C4514()
{
	_pinNbr = 24;
	for (unsigned int i = 0; i < _pinNbr; i++) {
		_pin[i] = nullptr;
		if (i == 23 || i == 11)
			_pinType[i] = nts::NONE;
		else if ((i <= 2) || (i >= 20 && i <= 22))
			_pinType[i] = nts::RECEIVER;
		else
			_pinType[i] = nts::TRANSMITTER;
	}
	_previous = nts::UNDEFINED;
	_first = nullptr;
	_outputGate[0] = 11;
	_outputGate[1] = 9;
	_outputGate[2] = 10;
	_outputGate[3] = 8;
	_outputGate[4] = 7;
	_outputGate[5] = 6;
	_outputGate[6] = 5;
	_outputGate[7] = 4;
	_outputGate[8] = 18;
	_outputGate[9] = 17;
	_outputGate[10] = 20;
	_outputGate[11] = 19;
	_outputGate[12] = 14;
	_outputGate[13] = 13;
	_outputGate[14] = 16;
	_outputGate[15] = 15;
}

C4514::~C4514()
{
}

void C4514::dump() const
{
	std::cout << "C4514" << std::endl;
}

std::size_t C4514::outputGateNbr()
{
	std::size_t gateNbr = 0;
	nts::Tristate inputA = this->compute(2);
	nts::Tristate inputB = this->compute(3);
	nts::Tristate inputC = this->compute(21);
	nts::Tristate inputD = this->compute(22);

	if (inputA == nts::UNDEFINED ||
		inputB == nts::UNDEFINED ||
		inputC == nts::UNDEFINED ||
		inputD == nts::UNDEFINED)
		return (std::size_t)-1;
	gateNbr += (inputA == nts::TRUE) ? 1 : 0;
	gateNbr += (inputB == nts::TRUE) ? 2 : 0;
	gateNbr += (inputC == nts::TRUE) ? 4 : 0;
	gateNbr += (inputD == nts::TRUE) ? 8 : 0;
	return _outputGate[gateNbr];
}

nts::Tristate C4514::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (_pin[i] == nullptr || _pin[i] == this)
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	std::size_t gateNbr = this->outputGateNbr();
	nts::Tristate strobe = this->compute(1);
	nts::Tristate inibit = this->compute(23);
	nts::Tristate tmp = _previous;
	if (_first == nullptr)
		_first = _pin[i];
	if (_first == _pin[i])
		_previous = strobe;
	if (strobe == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (strobe == nts::FALSE)
		return tmp;
	if (inibit == nts::TRUE)
		return nts::FALSE;
	else if (inibit == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (gateNbr == (size_t)-1 || gateNbr != i + 1)
		return nts::FALSE;
	return nts::TRUE;
}

void C4514::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4514::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4514::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}