/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <iostream>
#include <stdexcept>
#include "True.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"

True::True()
{
	_pin = nullptr;
}

True::~True()
{
}

void True::dump() const
{
	std::cout << "True" << std::endl;
}

nts::Tristate True::compute(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	return nts::Tristate::TRUE;
}

void True::unLink(std::size_t pin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	_pin = nullptr;
	_link = std::make_pair(0, 0);
}

void True::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin != 1)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin)
		throw nts::LogicError("Pin already binded.");
	_pin = &other;
	_link = std::make_pair(pin, oPin);
}

nts::PinType True::getPinType(std::size_t pin) const
{
	(void)pin;
	return nts::TRANSMITTER;
}