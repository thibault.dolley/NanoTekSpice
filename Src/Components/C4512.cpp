/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4512.hpp"
#include "NtsErrors.hpp"
#include "AComponent.hpp"
#include "IComponent.hpp"

C4512::C4512()
{
	for (unsigned int i = 0; i < 16; i++) {
		_pin[i] = nullptr;
		_pinType[i] = nts::RECEIVER;
		if (i == 15 || i == 7)
			_pinType[i] = nts::NONE;
		if (i == 13)
			_pinType[i] = nts::TRANSMITTER;
	}
}

C4512::~C4512()
{
}

void C4512::dump() const
{
	std::cout << "C4512" << std::endl;
}

std::size_t C4512::outputGateNbr()
{
	std::size_t gateNbr = 0;
	nts::Tristate inputA = this->compute(11);
	nts::Tristate inputB = this->compute(12);
	nts::Tristate inputC = this->compute(13);

	if (inputA == nts::UNDEFINED ||
		inputB == nts::UNDEFINED ||
		inputC == nts::UNDEFINED)
		return (std::size_t)-1;
	gateNbr += (inputA == nts::TRUE) ? 1 : 0;
	gateNbr += (inputB == nts::TRUE) ? 2 : 0;
	gateNbr += (inputC == nts::TRUE) ? 4 : 0;
	if (gateNbr + 1 == 8)
		return 9;
	return gateNbr + 1;	
}

nts::Tristate C4512::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i);

	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (_pin[i - 1] == nullptr)
			return nts::UNDEFINED;
		return _pin[i - 1]->compute(_link[i - 1].second);
	}
	nts::Tristate oeState = this->compute(15);
	nts::Tristate inibitState = this->compute(10);
	if (oeState != nts::FALSE || inibitState == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (inibitState == nts::TRUE)
		return nts::FALSE;
	std::size_t gateNbr = this->outputGateNbr();
	if (gateNbr == (std::size_t)-1)
		return nts::UNDEFINED;
	return this->compute(gateNbr);
}

void C4512::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 16)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4512::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > 16)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4512::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > 16)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}