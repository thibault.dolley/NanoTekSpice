/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <utility>
#include <iostream>
#include <stdexcept>
#include "C4017.hpp"
#include "NtsErrors.hpp"
#include "IComponent.hpp"

C4017::C4017()
{
	_pinNbr = 15;
	_counter = 1;
	for (unsigned int i = 0; i < _pinNbr; i++) {
		_pin[i] = nullptr;
		if (i == 12 || i == 13 || i == 14)
			_pinType[i] = nts::RECEIVER;
		else
			_pinType[i] = nts::TRANSMITTER;
	}
	_first = nullptr;
	_output[3] = 1;
	_output[2] = 2;
	_output[4] = 3;
	_output[7] = 4;
	_output[10] = 5;
	_output[1] = 6;
	_output[5] = 7;
	_output[6] = 8;
	_output[9] = 9;
	_output[11] = 10;
}

C4017::~C4017()
{
}

void C4017::dump() const
{
	std::cout << "C4017" << std::endl;
}

nts::Tristate C4017::computeC4017(std::size_t i)
{
	nts::Tristate clock1 = this->compute(13);
	nts::Tristate clock0 = this->compute(14);
	nts::Tristate reset = this->compute(15);

	if (reset == nts::UNDEFINED || clock0 == nts::UNDEFINED)
		return nts::UNDEFINED;
	if (reset == nts::TRUE) {
		_counter = 1;
		return nts::FALSE;
	}
	if (clock0 == nts::TRUE && clock1 != nts::TRUE)
		_counter++;
	_counter = (_counter > 10) ? 10 : _counter;
	if (_output[i] ==_counter)
		return nts::TRUE;
	return nts::FALSE;
}

nts::Tristate C4017::compute(std::size_t i)
{
	nts::PinType type = this->getPinType(i--);
	if (type == nts::NONE)
		return nts::UNDEFINED;
	if (type == nts::RECEIVER) {
		if (_pin[i] == nullptr || _pin[i] == this)
			return nts::UNDEFINED;
		return _pin[i]->compute(_link[i].second);
	}
	if (i + 1 == 12)
		return (_counter >= 5) ? nts::FALSE : nts::TRUE;
	if (_first == nullptr)
		_first = _pin[i];
	if (_first != _pin[i]) {
		if (_output[i + 1] ==_counter)
			return nts::TRUE;
		return nts::FALSE;
	}
	return this->computeC4017(i + 1);
}

void C4017::unLink(std::size_t pin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	_pin[pin - 1] = nullptr;
	_link[pin - 1] = std::make_pair(0, 0);
}

void C4017::setLink(std::size_t pin, nts::IComponent &other, std::size_t oPin)
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	if (_pin[pin - 1])
		throw nts::LogicError("Pin already binded.");
	_pin[pin - 1] = &other;
	_link[pin - 1] = std::make_pair(pin, oPin);
}

nts::PinType C4017::getPinType(std::size_t pin) const
{
	if (pin <= 0)
		throw nts::InvalidArg("Invalid pin number.");
	if (pin-- > _pinNbr)
		throw nts::InvalidArg("Invalid pin number.");
	return _pinType[pin];
}