/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include "True.hpp"
#include "C4001.hpp"
#include "C4008.hpp"
#include "C4011.hpp"
#include "C4013.hpp"
#include "C4017.hpp"
#include "C4030.hpp"
#include "C4040.hpp"
#include "C4069.hpp"
#include "C4071.hpp"
#include "C4081.hpp"
#include "C4512.hpp"
#include "C4514.hpp"
#include "Clock.hpp"
#include "Input.hpp"
#include "False.hpp"
#include "Output.hpp"
#include "NtsErrors.hpp"
#include "IComponent.hpp"
#include "ComponentManager.hpp"

ComponentManager::ComponentManager()
{
	_manager["true"] = MANAGER_BIND(&ComponentManager::createTrue);
	_manager["clock"] = MANAGER_BIND(&ComponentManager::createClock);
	_manager["input"] = MANAGER_BIND(&ComponentManager::createInput);
	_manager["false"] = MANAGER_BIND(&ComponentManager::createFalse);
	_manager["output"] = MANAGER_BIND(&ComponentManager::createOutput);
	_manager["terminal"] = MANAGER_BIND(&ComponentManager::createC4013);
	_manager["4001"] = MANAGER_BIND(&ComponentManager::createC4001);
	_manager["4008"] = MANAGER_BIND(&ComponentManager::createC4008);
	_manager["4011"] = MANAGER_BIND(&ComponentManager::createC4011);
	_manager["4013"] = MANAGER_BIND(&ComponentManager::createC4013);
	_manager["4017"] = MANAGER_BIND(&ComponentManager::createC4017);
	_manager["4030"] = MANAGER_BIND(&ComponentManager::createC4030);
	_manager["4040"] = MANAGER_BIND(&ComponentManager::createC4040);
	_manager["4069"] = MANAGER_BIND(&ComponentManager::createC4069);
	_manager["4071"] = MANAGER_BIND(&ComponentManager::createC4071);
	_manager["4081"] = MANAGER_BIND(&ComponentManager::createC4081);
	_manager["4512"] = MANAGER_BIND(&ComponentManager::createC4512);
	_manager["4514"] = MANAGER_BIND(&ComponentManager::createC4514);
}

ComponentManager::~ComponentManager()
{
}

std::unique_ptr<nts::IComponent> ComponentManager::createComponent(
						const std::string &name,
						const std::string &value)
{
	if (_manager.find(name) == _manager.end())
		throw nts::InvalidArg("Unknow component.");
	return std::unique_ptr<nts::IComponent>(_manager[name](value));
}


nts::IComponent *ComponentManager::createTrue(const std::string &str)
const noexcept
{
	(void)str;
	return new True;
}

nts::IComponent *ComponentManager::createFalse(const std::string &str)
const noexcept
{
	(void)str;
	return new False;
}

nts::IComponent *ComponentManager::createInput(const std::string &str)
const noexcept
{
	(void)str;
	return new Input;
}

nts::IComponent *ComponentManager::createOutput(const std::string &str)
const noexcept
{
	(void)str;
	return new Output;
}

nts::IComponent *ComponentManager::createClock(const std::string &str)
const noexcept
{
	(void)str;
	return new Clock;
}

nts::IComponent *ComponentManager::createC4001(const std::string &str)
const noexcept
{
	(void)str;
	return new C4001;
}

nts::IComponent *ComponentManager::createC4008(const std::string &str)
const noexcept
{
	(void)str;
	return new C4008;
}

nts::IComponent *ComponentManager::createC4011(const std::string &str)
const noexcept
{
	(void)str;
	return new C4011;
}

nts::IComponent *ComponentManager::createC4013(const std::string &str)
const noexcept
{
	(void)str;
	return new C4013;
}

nts::IComponent *ComponentManager::createC4017(const std::string &str)
const noexcept
{
	(void)str;
	return new C4017;
}

nts::IComponent *ComponentManager::createC4030(const std::string &str)
const noexcept
{
	(void)str;
	return new C4030;
}

nts::IComponent *ComponentManager::createC4040(const std::string &str)
const noexcept
{
	(void)str;
	return new C4040;
}

nts::IComponent *ComponentManager::createC4069(const std::string &str)
const noexcept
{
	(void)str;
	return new C4069;
}

nts::IComponent *ComponentManager::createC4071(const std::string &str)
const noexcept
{
	(void)str;
	return new C4071;
}

nts::IComponent *ComponentManager::createC4081(const std::string &str)
const noexcept
{
	(void)str;
	return new C4081;
}

nts::IComponent *ComponentManager::createC4512(const std::string &str)
const noexcept
{
	(void)str;
	return new C4512;
}

nts::IComponent *ComponentManager::createC4514(const std::string &str)
const noexcept
{
	(void)str;
	return new C4514;
}