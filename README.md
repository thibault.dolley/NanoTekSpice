# How To Nano

## What is the NanoTekSpice

The **NanoTekSpice** is a simulation of an electric circuit written in c++.
It include several component such as Input, Output, Clock, logic component and
several counter.

## Implemented component

### Special component

* Input: entry point of the circuit.
* Clock: Just like Input, but it's value change after each simulation
* True: Just like Input, but it's value is always true.
* False: The opposite of true.
* Output: end point of the circuit.

### Logical component

* 4001: Four NOR gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4001.pdf))
* 4011: Four NAND gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4011.pdf))
* 4030: Four XOR gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4030.pdf))
* 4069: Six INVERTER gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4069.pdf))
* 4071: Four OR gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4071.pdf))
* 4081: Four AND gates. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4081.pdf))

### Counter

* 4008: Four bits adder. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4008.pdf))
* 4040: 12 bits counter. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4040.pdf))

### Other

* 4514: Four bits Decoder. ([Documentation](https://intra.epitech.eu/module/2017/B-CPP-400/REN-4-1/acti-278580/project/file/tonsil/4514.pdf))

## Configuration file

In Order to use the NanoTekSpice, you will first need to create a configuration
file, you can find a lot of example in the Sample directory. Don't be affraid if
you are having trouble, error will all be reported smoothly so you can fix it
easily ;).
A configuration file looks like this:
	
	.chipsets:		# Start of .chipsets section.
	input			a
	input			b
	input			c
	input			d
	output			s1
	4081			4081

	.links:			# Start of .links section.
	a:1			4081:1
	b:1			4081:2
	c:1			4081:6
	d:1			4081:5
	4081:3 			4081:12
	4081:4 			4081:13
	4081:11			s1:1

'#' Are used to introduce comment in the configuration file.

### The '.chipsets:' section

This section is where you will declare and name the component you are going to
use during the simulation. This section must be introduced before the *.links:*
section.

In order to create a component, you first need to write it's type and then give
it a name. You can create as much component as you like.

### The '.links:' section

This section is where you will define the link between all the components
declared previously. To set up a link just call the components and define the
number of the pin you wish to links. 

## The command line

After creating your configuration lines, you will have to start the program
through a command line like this:

	./nanotekspice my_configuration.nts a=0 b=1 c=0 d=1

The command line is rather easy to understand: the first param is the
configuration file. Then you have to affect a value (true or false) to some
component. Those component are none other than *Input* and *Clock*. These are 
needed as otherwise the result of the simulation will be undefined.

## The NanoShell

After running the simulation once and displaying the result, you will be
greeted with a prompt. This prompt will allow you to perform some action over
the circuit.

### Command avaible

* simulate: run another simulation.
* display: display the state of the output.
* loop: run the simulation until the SIGINT is received.
* dump: dump all components from the circuit.
* *component=value*: setup once again the value of a *Clock* or an *Input*. 
* exit: exit the nanotekspice.