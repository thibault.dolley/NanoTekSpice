/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4008.hpp"
#include "Input.hpp"

Test(C4008, computeUndefinedPin)
{
	C4008 _c4008;

	for (int i = 0; i < 16; i++)
		cr_assert_eq(_c4008.compute(i + 1), (-true));
}

Test(C4008, getPinType)
{
	C4008 _c4008;
	nts::PinType type;

	for (int i = 1; i < 17; i++) {
		if (i == 8 || i == 16)
			type = nts::NONE;
		else if (i <= 9 || i == 15)
			type = nts::RECEIVER;
		else
			type = nts::TRANSMITTER;
		cr_assert_eq(_c4008.getPinType(i), type);
	}
}

Test(C4008, allSetLinkCase)
{
	C4008 _c4008;

	try {
		_c4008.setLink(25, _c4008, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4008.setLink(1, _c4008, 1);
	try {
		_c4008.setLink(1, _c4008, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}