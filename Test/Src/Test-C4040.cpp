/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4040.hpp"
#include "Input.hpp"

Test(C4040, computeUndefinedPin)
{
	C4040 _c4040;

	for (int i = 0; i < 16; i++)
		_c4040.compute(i + 1);
}

Test(C4040, getPinType)
{
	C4040 _c4040;
	nts::PinType type;

	for (int i = 1; i <= 16; i++) {
		if (i == 10 || i == 11)
			type = nts::RECEIVER;
		else if (i == 16 || i == 8)
			type = nts::NONE;
		else
			type = nts::TRANSMITTER;
		cr_assert_eq(_c4040.getPinType(i), type);
	}
}

Test(C4040, allSetLinkCase)
{
	C4040 _c4040;

	try {
		_c4040.setLink(17, _c4040, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4040.setLink(1, _c4040, 1);
	try {
		_c4040.setLink(1, _c4040, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}
