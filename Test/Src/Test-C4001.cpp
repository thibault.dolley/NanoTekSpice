/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4001.hpp"
#include "Input.hpp"

Test(C4001, computeUndefinedPin)
{
	C4001 _c4001;

	for (int i = 0; i < 14; i++)
		cr_assert_eq(_c4001.compute(i + 1), (-true));
}

Test(C4001, getPinType)
{
	C4001 _c4001;
	nts::PinType type;

	for (int i = 1; i < 15; i++) {
		if (i == 3 || i == 4 || i == 11 || i == 10)
			type = nts::TRANSMITTER;
		else if (i == 7 || i == 14)
			type = nts::NONE;
		else
			type = nts::RECEIVER;
		cr_assert_eq(_c4001.getPinType(i), type);
	}
}

Test(C4001, allSetLinkCase)
{
	C4001 _c4001;

	try {
		_c4001.setLink(15, _c4001, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4001.setLink(1, _c4001, 1);
	try {
		_c4001.setLink(1, _c4001, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(C4001, computeAllCaseGate1)
{
	Input i1;
	Input i2;
	C4001 _c4001;

	_c4001.setLink(1, i1, 1);
	_c4001.setLink(2, i2, 1);
	i1.setLink(1, _c4001, 1);
	i2.setLink(1, _c4001, 2);
	cr_assert_eq(_c4001.compute(3), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4001.compute(3), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4001.compute(3), (false));
	i2.setValue(0);
	cr_assert_eq(_c4001.compute(3), (false));
	i1.setValue(0);
	cr_assert_eq(_c4001.compute(3), (true));
}

Test(C4001, computeAllCaseGate2)
{
	Input i1;
	Input i2;
	C4001 _c4001;

	_c4001.setLink(5, i1, 1);
	_c4001.setLink(6, i2, 1);
	i1.setLink(1, _c4001, 5);
	i2.setLink(1, _c4001, 6);
	cr_assert_eq(_c4001.compute(4), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4001.compute(4), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4001.compute(4), (false));
	i2.setValue(0);
	cr_assert_eq(_c4001.compute(4), (false));
	i1.setValue(0);
	cr_assert_eq(_c4001.compute(4), (true));
}

Test(C4001, computeAllCaseGate3)
{
	Input i1;
	Input i2;
	C4001 _c4001;

	_c4001.setLink(8, i1, 1);
	_c4001.setLink(9, i2, 1);
	i1.setLink(1, _c4001, 8);
	i2.setLink(1, _c4001, 9);
	cr_assert_eq(_c4001.compute(10), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4001.compute(10), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4001.compute(10), (false));
	i2.setValue(0);
	cr_assert_eq(_c4001.compute(10), (false));
	i1.setValue(0);
	cr_assert_eq(_c4001.compute(10), (true));
}

Test(C4001, computeAllCaseGate4)
{
	Input i1;
	Input i2;
	C4001 _c4001;

	_c4001.setLink(13, i1, 1);
	_c4001.setLink(12, i2, 1);
	i1.setLink(1, _c4001, 13);
	i2.setLink(1, _c4001, 12);
	cr_assert_eq(_c4001.compute(11), (-true));
	i1.setValue(1);
	cr_assert_eq(_c4001.compute(11), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4001.compute(11), (false));
	i2.setValue(0);
	cr_assert_eq(_c4001.compute(11), (false));
	i1.setValue(0);
	cr_assert_eq(_c4001.compute(11), (true));
}