/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4011.hpp"
#include "Input.hpp"

Test(C4011, computeUndefinedPin)
{
	C4011 _c4011;

	for (int i = 0; i < 14; i++)
		cr_assert_eq(_c4011.compute(i + 1), (-true));
}

Test(C4011, getPinType)
{
	C4011 _c4011;
	nts::PinType type;

	for (int i = 1; i < 15; i++) {
		if (i == 3 || i == 4 || i == 11 || i == 10)
			type = nts::TRANSMITTER;
		else if (i == 7 || i == 14)
			type = nts::NONE;
		else
			type = nts::RECEIVER;
		cr_assert_eq(_c4011.getPinType(i), type);
	}
}

Test(C4011, allSetLinkCase)
{
	C4011 _c4011;

	try {
		_c4011.setLink(15, _c4011, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4011.setLink(1, _c4011, 1);
	try {
		_c4011.setLink(1, _c4011, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(C4011, computeAllCaseGate1)
{
	Input i1;
	Input i2;
	C4011 _c4011;

	_c4011.setLink(1, i1, 1);
	_c4011.setLink(2, i2, 1);
	i1.setLink(1, _c4011, 1);
	i2.setLink(1, _c4011, 2);
	cr_assert_eq(_c4011.compute(3), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(3), (true));
	i1.setValue(1);
	i1.setValue(1);
	cr_assert_eq(_c4011.compute(3), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4011.compute(3), (false));
	i2.setValue(0);
	cr_assert_eq(_c4011.compute(3), (true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(3), (true));
}

Test(C4011, computeAllCaseGate2)
{
	Input i1;
	Input i2;
	C4011 _c4011;

	_c4011.setLink(5, i1, 1);
	_c4011.setLink(6, i2, 1);
	i1.setLink(1, _c4011, 5);
	i2.setLink(1, _c4011, 6);
	cr_assert_eq(_c4011.compute(4), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(4), (true));
	i1.setValue(1);
	i1.setValue(1);
	cr_assert_eq(_c4011.compute(4), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4011.compute(4), (false));
	i2.setValue(0);
	cr_assert_eq(_c4011.compute(4), (true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(4), (true));
}

Test(C4011, computeAllCaseGate3)
{
	Input i1;
	Input i2;
	C4011 _c4011;

	_c4011.setLink(8, i1, 1);
	_c4011.setLink(9, i2, 1);
	i1.setLink(1, _c4011, 8);
	i2.setLink(1, _c4011, 9);
	cr_assert_eq(_c4011.compute(10), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(10), (true));
	i1.setValue(1);
	i1.setValue(1);
	cr_assert_eq(_c4011.compute(10), (-true));
	i2.setValue(1);
	cr_assert_eq(_c4011.compute(10), (false));
	i2.setValue(0);
	cr_assert_eq(_c4011.compute(10), (true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(10), (true));
}

Test(C4011, computeAllCaseGate4)
{
	Input i1;
	Input i2;
	C4011 _c4011;

	_c4011.setLink(13, i1, 1);
	_c4011.setLink(12, i2, 1);
	i1.setLink(1, _c4011, 13);
	i2.setLink(1, _c4011, 12);
	cr_assert_eq(_c4011.compute(11), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(11), (true));
	i1.setValue(1);
	i2.setValue(1);
	cr_assert_eq(_c4011.compute(11), (false));
	i2.setValue(0);
	cr_assert_eq(_c4011.compute(11), (true));
	i1.setValue(0);
	cr_assert_eq(_c4011.compute(11), (true));
}