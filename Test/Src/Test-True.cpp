/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "True.hpp"

Test(True, compute)
{
	True _true;

	cr_assert(_true.compute(1) == true);
}

Test(True, computeError)
{
	True _true;

	try {
		_true.compute(2);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(True, getPinType)
{
	True _true;

	cr_assert(_true.getPinType(1) == nts::TRANSMITTER);
}

Test(True, allSetLinkCase)
{
	True _true;

	try {
		_true.setLink(2, _true, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_true.setLink(1, _true, 1);
	try {
		_true.setLink(1, _true, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}