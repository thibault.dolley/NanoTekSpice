/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4013.hpp"
#include "Input.hpp"

Test(C4013, computeUndefinedPin)
{
	C4013 _c4013;

	for (int i = 0; i < 14; i++)
		cr_assert_eq(_c4013.compute(i + 1), (-true));
}

Test(C4013, getPinType)
{
	C4013 _c4013;
	nts::PinType type;

	for (int i = 1; i < 15; i++) {
		if (i == 14 || i == 7)
			type = nts::NONE;
		else if (i == 1 || i == 2 || i == 12 || i == 13)
			type = nts::TRANSMITTER;
		else
			type = nts::RECEIVER;
		cr_assert_eq(_c4013.getPinType(i), type);
	}
}

Test(C4013, allSetLinkCase)
{
	C4013 _c4013;

	try {
		_c4013.setLink(15, _c4013, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4013.setLink(1, _c4013, 1);
	try {
		_c4013.setLink(1, _c4013, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}