/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "Clock.hpp"

Test(Clock, computeAndSetValue)
{
	Clock _clock;

	cr_assert(_clock.compute(1) == (-true));
	_clock.setValue(1);
	cr_assert(_clock.compute(1) == true);
	_clock.setValue(0);
	cr_assert(_clock.compute(1) == false);
}

Test(Clock, computeAndInvert)
{
	Clock _clock;

	_clock.setValue(1);
	cr_assert(_clock.compute(1) == true);
	_clock.invertValue();
	cr_assert(_clock.compute(1) == false);	
	_clock.invertValue();
	cr_assert(_clock.compute(1) == true);
}

Test(Clock, computeError)
{
	Clock _clock;

	try {
		_clock.compute(2);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(Clock, getPinType)
{
	Clock _clock;

	cr_assert(_clock.getPinType(1) == nts::TRANSMITTER);
}

Test(Clock, allSetLinkCase)
{
	Clock _clock;

	try {
		_clock.setLink(2, _clock, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_clock.setLink(1, _clock, 1);
	try {
		_clock.setLink(1, _clock, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}