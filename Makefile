#
# EPITECH PROJECT, 2018
# singe
# File description:
# singe
#

NAME		=	nanotekspice

TESTDIR 	=	Test/

CC		=	g++

CPPFLAGS	=	-Wall -Wextra
CPPFLAGS	+=	-IInclude

SRC 		=	Src/
ERRORS		=	$(SRC)Errors/
COMPONENTS	=	$(SRC)Components/
SRCS		=					\
			$(SRC)Main.cpp 			\
			$(SRC)NanoTekSpice.cpp 		\
			$(SRC)FileHelper.cpp 		\
			$(SRC)StringHelper.cpp 		\
			$(SRC)NanoShell.cpp 		\
			$(SRC)NanoHelper.cpp 		\
			$(SRC)ComponentManager.cpp 	\
			$(ERRORS)NtsError.cpp 		\
			$(ERRORS)LogicError.cpp 	\
			$(ERRORS)InvalidArg.cpp 	\
			$(COMPONENTS)True.cpp 		\
			$(COMPONENTS)False.cpp 		\
			$(COMPONENTS)Input.cpp 		\
			$(COMPONENTS)Clock.cpp 		\
			$(COMPONENTS)C4001.cpp 		\
			$(COMPONENTS)C4008.cpp 		\
			$(COMPONENTS)C4011.cpp 		\
			$(COMPONENTS)C4013.cpp 		\
			$(COMPONENTS)C4017.cpp 		\
			$(COMPONENTS)C4030.cpp 		\
			$(COMPONENTS)C4040.cpp 		\
			$(COMPONENTS)C4069.cpp 		\
			$(COMPONENTS)C4071.cpp 		\
			$(COMPONENTS)C4081.cpp 		\
			$(COMPONENTS)C4512.cpp 		\
			$(COMPONENTS)C4514.cpp 		\
			$(COMPONENTS)Output.cpp 	\

OBJS 		=	$(SRCS:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS)

clean: 
	cd $(TESTDIR) && $(MAKE) clean
	rm -f $(OBJS)

fclean: clean
	cd $(TESTDIR) && $(MAKE) fclean
	rm -f $(NAME)

gcov:
	cd $(TESTDIR) && $(MAKE) gcov

test:
	cd $(TESTDIR) && $(MAKE)

tests_run:
	cd $(TESTDIR) && $(MAKE) re
	cd $(TESTDIR) && ./test

re: fclean $(NAME)